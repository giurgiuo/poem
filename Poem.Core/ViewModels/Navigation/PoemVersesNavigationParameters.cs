﻿using System;

namespace Poem.Core
{
	public class PoemVersesNavigationParameters
	{
		public string SectionName { get; set; }
	}
}

