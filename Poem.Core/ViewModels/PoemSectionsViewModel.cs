﻿using System;
using MvvmCross.Core.ViewModels;
using System.Collections.ObjectModel;

namespace Poem.Core.ViewModels
{
	public class PoemSectionsViewModel : MvxViewModel
	{
		private IPoemService _poemService;

		public PoemSectionsViewModel (IPoemService poemService)
		{
			_poemService = poemService;
		}

		public void Init()
		{
			// TODO: log info start

			// This normally should be provided by user
			const string poemJsonFileName = "TheRimeOfTheAncientMariner";
			PopulateSectionsAsync(poemJsonFileName);

			// TODO: log info finish
		}

		public async void PopulateSectionsAsync(string file)
		{
			// TODO: log info populate

			var poem = await _poemService.GetPoemAsync(file);
			var document = poem.Document;
			var content = document.Content;
			var sections = content.Sections;
			Sections = new ObservableCollection<Section>(sections);
		}

		#region Sections

		private ObservableCollection<Section> _sections;

		public ObservableCollection<Section> Sections
		{
			get { return _sections; }
			set 
			{
				if (_sections == value) return;

				_sections = value;
				RaisePropertyChanged (() => Sections);
			}
		}

		#endregion

		#region SelectedSection

		private Section _selectedSection;

		public Section SelectedSection
		{
			get { return _selectedSection; }
			set 
			{
				if (_selectedSection == value) return;

				_selectedSection = value;
				RaisePropertyChanged (() => SelectedSection);
			}
		}

		#endregion

		#region SelectSectionCommand

		private IMvxCommand _selectSectionCommand;

		public IMvxCommand SelectSectionCommand
		{
			get
			{
				_selectSectionCommand = _selectSectionCommand ?? new MvxCommand(SelectSection);
				return _selectSectionCommand;
			}
		}

		private void SelectSection()
		{
			// TODO: log info navigate to

			RaisePropertyChanged (() => SelectedSection);

			if (SelectedSection == null) 
			{
				// TODO: log error

				// TODO: User dialog message 

				return;
			}

			var poemSectionNavigationParameters = new PoemVersesNavigationParameters 
			{
				SectionName = SelectedSection.Name
			};

			ShowViewModel<PoemVersesViewModel> (poemSectionNavigationParameters);
		}

		#endregion
	}
}

