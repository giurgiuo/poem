﻿using System;
using System.Linq;
using MvvmCross.Core.ViewModels;
using System.Collections.Generic;

namespace Poem.Core
{
	public class PoemVersesViewModel : MvxViewModel
	{
		private IPoemService _poemService;

		public PoemVersesViewModel (IPoemService poemService)
		{
			_poemService = poemService;
		}

		private string _sectionName;

		public void Init(PoemVersesNavigationParameters poemVersesNavigationParameters)
		{
			// TODO: log info start

			#region Guard

			if(poemVersesNavigationParameters == null)
			{
				// TODO: log error

				Close(this);
				return;
			}

			#endregion

			_sectionName = poemVersesNavigationParameters.SectionName;

			const string poemJsonFileName = "TheRimeOfTheAncientMariner";
			PopulateVersesAsync (poemJsonFileName);

			// TODO: log info finish
		}

		public async void PopulateVersesAsync(string file)
		{
			// TODO: log info populate

			var poem = await _poemService.GetPoemAsync(file);
			var document = poem.Document;
			var content = document.Content;
			var sections = content.Sections;
			SelectedSection = sections.FirstOrDefault(s => s.Name == _sectionName);
			Verses = SelectedSection.Verses;
		}

		#region SelectedSection

		private Section _selectedSection;

		public Section SelectedSection
		{
			get { return _selectedSection; }
			set 
			{
				if (_selectedSection == value) return;

				_selectedSection = value;
				RaisePropertyChanged (() => SelectedSection);
			}
		}

		#endregion

		#region Verses

		private List<string> _verses;

		public List<string> Verses
		{
			get { return _verses; }
			set 
			{
				if (_verses == value) return;

				_verses = value;
				RaisePropertyChanged (() => Verses);
			}
		}

		#endregion
	}
}

