﻿using System;
using Newtonsoft.Json;

namespace Poem.Core
{
	public class RootObject
	{

		[JsonProperty("document")]
		public Document Document { get; set; }
	}
}

