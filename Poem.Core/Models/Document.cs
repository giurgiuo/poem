﻿using System;
using Newtonsoft.Json;

namespace Poem.Core
{
	public class Document
	{

		[JsonProperty("name")]
		public string Name { get; set; }

		[JsonProperty("author")]
		public string Author { get; set; }

		[JsonProperty("contents")]
		public Content Content { get; set; }
	}
}

