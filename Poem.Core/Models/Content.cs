﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Poem.Core
{
	public class Content
	{
		[JsonProperty("prelude")]
		public string Prelude { get; set; }

		[JsonProperty("section")]
		public List<Section> Sections { get; set;}
	}
}

