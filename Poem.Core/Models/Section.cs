﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Poem.Core
{
	public class Section
	{
		[JsonProperty("verse")]
		public List<string> Verses { get; set; }

		[JsonProperty("_name")]
		public string Name { get; set; }
	}
}

