﻿using System;
using MvvmCross.Platform.IoC;

namespace Poem.Core
{
	public class App : MvvmCross.Core.ViewModels.MvxApplication
	{
		public override void Initialize()
		{
			CreatableTypes ()
				.EndingWith ("Service")
				.AsInterfaces ()
				.ExcludeInterfaces(typeof(IPoemService))
				.RegisterAsLazySingleton ();

			RegisterAppStart<ViewModels.PoemSectionsViewModel>();
		}
	}
}

