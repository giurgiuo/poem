﻿using System;
using System.Threading.Tasks;

namespace Poem.Core
{
	public interface IPoemService
	{
		Task<RootObject> GetPoemAsync(string name);
	}
}

