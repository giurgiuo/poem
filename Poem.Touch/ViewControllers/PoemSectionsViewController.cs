using System;
using MvvmCross.Binding.BindingContext;
using MvvmCross.iOS.Views;
using Poem.Core.ViewModels;
using UIKit;
using Foundation;
using MvvmCross.Binding.iOS.Views;

namespace Poem.Touch
{
	[MvxFromStoryboard("Sections")]
	public partial class PoemSectionsViewController : MvxViewController
	{
		public PoemSectionsViewController(IntPtr handle) : base(handle)
		{
			Title = "Sections";
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad ();

			var source = new MvxSimpleTableViewSource (SectionsTable, SectionCell.Key, SectionCell.Key);
			SectionsTable.Source = source;

			var set = this.CreateBindingSet<PoemSectionsViewController, PoemSectionsViewModel>();
			set.Bind(source).To(vm => vm.Sections);
			set.Bind (source).For (x => x.SelectedItem).To (vm => vm.SelectedSection);
			set.Bind (source).For (x => x.SelectionChangedCommand).To (vm => vm.SelectSectionCommand);
			set.Apply ();

			SectionsTable.ReloadData();
		}
	}
}

