// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace Poem.Touch
{
	[Register ("PoemVersesViewController")]
	partial class PoemVersesViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UILabel SectionNameLabel { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextView VersesText { get; set; }

		void ReleaseDesignerOutlets ()
		{
			if (SectionNameLabel != null) {
				SectionNameLabel.Dispose ();
				SectionNameLabel = null;
			}
			if (VersesText != null) {
				VersesText.Dispose ();
				VersesText = null;
			}
		}
	}
}
