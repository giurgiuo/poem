using System;
using Foundation;
using MvvmCross.iOS.Views;
using MvvmCross.Binding.BindingContext;
using Poem.Core;
using System.CodeDom.Compiler;
using UIKit;

namespace Poem.Touch
{
	[MvxFromStoryboard("Verses")]
	public partial class PoemVersesViewController : MvxViewController
	{
		public PoemVersesViewController (IntPtr handle) : base(handle)
		{
			Title = "Verses";
		}

		public override void ViewDidLoad()
		{
			base.ViewDidLoad();

			var set = this.CreateBindingSet<PoemVersesViewController, PoemVersesViewModel> ();

			set.Bind (SectionNameLabel).To (vm => vm.SelectedSection.Name);
			set.Bind (VersesText).To (vm => vm.Verses).WithConversion(new VersesListToStringConverter());

			set.Apply ();
		}
	}
}

