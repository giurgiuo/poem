﻿using System;
using System.IO;
using MvvmCross.Platform;
using Poem.Core;
using MvvmCross.Platform.Platform;
using MvvmCross.iOS.Platform;
using Foundation;

namespace Poem.Touch
{
	public static class DependenciesSetup
	{
		public static void Setup()
		{
			var documentsPath = Environment.GetFolderPath (Environment.SpecialFolder.Personal);
			var libraryPath = Path.Combine (documentsPath, "..", "Library");

			Mvx.RegisterSingleton<IPoemService>(() => new JsonPoemService());

		}
	}
}

