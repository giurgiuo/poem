using MvvmCross.Platform.Plugins;

namespace Poem.Touch.Bootstrap
{
    public class JsonPluginBootstrap
        : MvxPluginBootstrapAction<MvvmCross.Plugins.Json.PluginLoader>
    {
    }
}