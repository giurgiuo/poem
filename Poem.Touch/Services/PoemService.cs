﻿using System;
using Poem.Core;
using System.Threading.Tasks;
using System.IO;
using Newtonsoft.Json;
using Foundation;

namespace Poem.Touch
{
	public class JsonPoemService : IPoemService
	{
		public async Task<RootObject> GetPoemAsync(string name)
		{
			if (name == null) 
			{
				throw new ArgumentNullException("name");
			}

			var fileName = NSBundle.MainBundle.PathForResource (name, "json");

			using (var fileStream = File.OpenRead(fileName))
			using (var reader = new StreamReader (fileStream)) 
			{
				var poemJson = await reader.ReadToEndAsync();
				var poem = JsonConvert.DeserializeObject<RootObject> (poemJson);
				return poem;
			}
		}
	}
}

