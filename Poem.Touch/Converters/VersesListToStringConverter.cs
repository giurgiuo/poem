﻿using System;
using MvvmCross.Platform.Converters;
using System.Collections.Generic;
using System.Text;
using System.Globalization;

namespace Poem.Touch
{
	public class VersesListToStringConverter : MvxValueConverter<List<string>, string>
	{
		protected override string Convert(List<string> value, Type targetType, object parameter, CultureInfo cultureInfo)
		{
			if (value == null) return string.Empty;

			var stringBuilder = new StringBuilder();

			foreach (var str in value) 
			{
				stringBuilder.AppendLine (str);
			}

			return stringBuilder.ToString();
		}
	}
}

