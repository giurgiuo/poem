﻿using System;

using Foundation;
using UIKit;
using MvvmCross.Binding.iOS.Views;
using MvvmCross.Binding.BindingContext;
using Poem.Core;

namespace Poem.Touch
{
	public partial class SectionCell : MvxTableViewCell
	{
		public static readonly NSString Key = new NSString ("SectionCell");
		public static readonly UINib Nib = UINib.FromName ("SectionCell", NSBundle.MainBundle);

		public SectionCell (IntPtr handle) : base (handle)
		{
			this.DelayBind (() => {
				var set = this.CreateBindingSet<SectionCell, Section>();
				set.Bind(SectionCellLabel).To(s => s.Name);
				set.Apply();
			});
		}

		public static SectionCell Create()
		{
			return (SectionCell)Nib.Instantiate (null, null) [0];
		}
	}
}
